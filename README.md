# session-token-jwt

#### 介绍
session、token、jwt的区别

###
session存在内存里面，session的id是存放在cookie里面，cookie是存放在浏览器里面，浏览器关闭后，cookie会消失，session也会消失。

###
token存在redis里面，token的id是存放在cookie里面，cookie是存放在浏览器里面，浏览器关闭后，cookie会消失，token不会消失。

###
jwt存在redis里面，jwt的id是存放在cookie里面，cookie是存放在浏览器里面，浏览器关闭后，cookie会消失，jwt也不会消失。
jwt更多的是像对用户信息进行验证,没办法实现对用户的踢出操作。可能有类似的操作，但是我看的视频说没有实现。
如果需要实现，就需要借助redis，那么还不如用第二种方法。
我理解的jwt就是对称加密。
视频地址:https://www.bilibili.com/video/BV1Hc411m7Ma/?p=4&spm_id_from=pageDriver&vd_source=b4c76352d47653735f3eb4090e4c95d4
