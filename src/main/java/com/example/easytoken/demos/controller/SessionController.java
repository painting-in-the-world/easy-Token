package com.example.easytoken.demos.controller;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.example.easytoken.demos.utils.RedisCache;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class SessionController {
    @Resource
    private RedisCache cache;
    
    @RequestMapping("saveSession")
    public String saveSession(HttpSession session,String message){
        if (StrUtil.isBlank(session.getId())){
            return "session为空";
        }
        session.setAttribute("message",message);
        return "sessionId为" + session.getId();
        
    }
    
    @RequestMapping("/getSession")
    public String getSession(HttpSession session){
        session.getAttribute("message");
        return "sessionId为:"+session.getAttribute("message");
    }

    @RequestMapping("saveByToken")
    public String saveByToken(String msg){
        if (StrUtil.isBlank(msg)) {
            return "msg为空";
        } else {
            val token = IdUtil.simpleUUID();
            log.error("保存token为:" + token);
            cache.setCacheObject(token,msg,10, TimeUnit.SECONDS);
            return "保存成功,token为:"+ token;
        }
    }
    @RequestMapping("getByToken")
    public String getByToken(String token){
        if (StrUtil.isBlank(token)) {
            return "token为空";
        } else {
            val msg = cache.getCacheObject(token);
            if (msg == null) {
                return "token不存在";
            } else {
                return "token为:" + token + ",msg为:" + msg;
            }
        }
    }
}
