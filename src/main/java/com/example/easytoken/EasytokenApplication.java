package com.example.easytoken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasytokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasytokenApplication.class, args);
    }

}
